import GridLayout from 'react-grid-layout';
import React, { Component } from 'react';
import ToggleButton from 'react-toggle-button'
import {Button} from 'react-bootstrap';
import openSocket from 'socket.io-client';
const socket = openSocket('http://localhost:3000');

class MyFirstGrid extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      mains: false,
      sample1: false,
      sample2: false,
      sample3: false,
      sample4: false,
      sample5: false,
      oc1: false,
      oc2: false,
      oc3: false,
      oc4: false,
      oc5: false,
      started: false,
      elsapsedTime: 0
      };
  
    this.startTest = this.startTest.bind(this);
    this.endTest = this.endTest.bind(this);
    this.gotTime = this.gotTime.bind(this);
  }
  
  startTest = function() {
    this.setState({
      started: true, 
      sample1: true, 
      sample2: true, 
      sample3: true, 
      sample4: true, 
      sample5: true, 
      oc1: false, 
      oc2: false, 
      oc3: false, 
      oc4: false, 
      oc5: false
    });
    socket.emit("startAutoTest", "");
    socket.on("time", (msg) => this.gotTime(msg))
  }

  endTest = function() {
    this.setState({
      started: false, 
      sample1: false, 
      sample2: false, 
      sample3: false, 
      sample4: false, 
      sample5: false, 
      oc1: false, 
      oc2: false, 
      oc3: false, 
      oc4: false, 
      oc5: false
    });
    socket.emit("stopAutoTest", "");
  }

  gotTime = function(time) {
    if (this.state.started) {
      this.setState({elsapsedTime: time})
    }
  }

  formatSeconds = function(seconds) {
      var date = new Date(1970,0,1);
      date.setSeconds(seconds);
      return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
  }

  render() {
    var totalSeconds = this.state.elsapsedTime
    var hours = Math.floor(totalSeconds / 3600);
    totalSeconds %= 3600;
    var minutes = Math.floor(totalSeconds / 60);
    var seconds = totalSeconds % 60;
    seconds = Math.floor(seconds);

    var oc1v = this.state.oc1 ? "visible" : "hidden";
    var oc2v = this.state.oc2 ? "visible" : "hidden";
    var oc3v = this.state.oc3 ? "visible" : "hidden";
    var oc4v = this.state.oc4 ? "visible" : "hidden";
    var oc5v = this.state.oc5 ? "visible" : "hidden";
    // layout is an array of objects, see the demo for more complete usage
    var layout = [
      {i: 'a', x: 0, y: 0, w: 4, h: 10,},
      {i: 'b', x: 0, y: 10, w: 4, h: 10, minW: 2, maxW: 4},
      {i: 'c', x: 4, y: 0, w: 6, h: 10, minW: 6, minH: 12},
      {i: 'd', x: 4, y: 10, w: 6, h: 10, minW: 6, minH: 12},
      {i: 'e', x: 10, y: 0, w: 2, h: 4, minW: 2, minH: 4}

    ];
    return (
      <GridLayout className="layout" layout={layout} cols={12} rowHeight={30} width={1200}>
        <div key="a">
        <h3>Automatic Test Sequence</h3>
        <p>The following procedure should be carried out:</p>
        <p>1. Enable mains relay</p>
        <p>2. Set the variac voltage</p>
        <p>3. Start the automated test</p>
        <br></br>

    
    <Button bsStyle="success" onClick={this.startTest} disabled={!this.state.mains}>Start Automated Test</Button>
    <br></br>
    <br></br>
    <Button onClick={this.endTest} bsStyle="danger">Stop All</Button>
      
      <br></br>
      <br></br>

      <p>Elapsed Time</p>
      <h3 style={{padding: "0px"}}><strong>{this.formatSeconds(this.state.elsapsedTime)}</strong></h3>

        </div>
        <div key="b">
        <h3>Manual Controls and Status</h3>
    
    <h4 style={{display: "inline-block"}}>Mains Relay:</h4>
    <div style={{display: "inline-block", marginLeft: "10px"}}>
    <ToggleButton 
      value={ this.state.mains || false }
      onToggle={(value) => {
        socket.emit("mainsOn", "")
        this.setState({
          mains: !this.state.mains,
        })
    }} />
    </div>
      <br></br>
      <h4 style={{display: "inline-block"}}>Sample 1:</h4>
      <div style={{display: "inline-block", marginLeft: "10px"}}>
      <ToggleButton 
        value={ this.state.sample1 || false }
        onToggle={(value) => {
          socket.emit("enableRelay", "1")
          this.setState({
            sample1: !this.state.sample1,
          })
      }} />
      </div>
      <Button bsStyle="warning" style={{padding: "0px", marginLeft: "5px", visibility: oc1v}}>Overcurrent</Button>

      <br></br>
      <h4 style={{display: "inline-block"}}>Sample 2:</h4>
      <div style={{display: "inline-block", marginLeft: "10px"}}>
      <ToggleButton 
        value={ this.state.sample2 || false }
        onToggle={(value) => {
          socket.emit("enableRelay", "2")
          this.setState({
            sample2: !this.state.sample2,
          })
      }} />
      </div>
      <Button bsStyle="warning" style={{padding: "0px", marginLeft: "5px", visibility: oc2v}}>Overcurrent</Button>

      <br></br>
      <h4 style={{display: "inline-block"}}>Sample 3:</h4>
      <div style={{display: "inline-block", marginLeft: "10px"}}>
      <ToggleButton 
        value={ this.state.sample3 || false }
        onToggle={(value) => {
          socket.emit("enableRelay", "3")
          this.setState({
            sample3: !this.state.sample3,
          })
      }} />
      </div>
      <Button bsStyle="warning" style={{padding: "0px", marginLeft: "5px", visibility: oc3v}}>Overcurrent</Button>

      <br></br>
      <h4 style={{display: "inline-block"}}>Sample 4:</h4>
      <div style={{display: "inline-block", marginLeft: "10px"}}>
      <ToggleButton 
        value={ this.state.sample4 || false }
        onToggle={(value) => {
          socket.emit("enableRelay", "4")
          this.setState({
            sample4: !this.state.sample4,
          })
      }} />
      </div>
      <Button bsStyle="warning" style={{padding: "0px", marginLeft: "5px", visibility: oc4v}}>Overcurrent</Button>

      <br></br>
      <h4 style={{display: "inline-block"}}>Sample 5:</h4>
      <div style={{display: "inline-block", marginLeft: "10px"}}>
      <ToggleButton 
        value={ this.state.sample5 || false }
        onToggle={(value) => {
          socket.emit("enableRelay", "5")
          this.setState({
            sample5: !this.state.sample5,
          })
      }} />
      </div>
      <Button bsStyle="warning" style={{padding: "0px", marginLeft: "5px", visibility: oc5v}}>Overcurrent</Button>
    
        </div>
        <div key="c" style={{overflow: "none", height: "100%", width: "100%"}}>
        <h3>Live Video</h3>
        <div style = {{background: "#999", margin: "20px", height: "-webkit-fill-available", width: "-webkit-fill-available", webkitMaxLogicalHeight: "80%", textAlign: "center"}}></div>
        </div>

        <div key="d" style={{overflow: "none", height: "100%", width: "100%"}}>
        <h3>Current Monitoring</h3>
        <div style = {{background: "#eee", margin: "20px", height: "-webkit-fill-available", width: "-webkit-fill-available", webkitMaxLogicalHeight: "80%", textAlign: "center"}}></div>
        </div>

        <div key="e" style={{overflow: "none", height: "100%", width: "100%"}}>
        <h3>Angle Change</h3>
        <br></br>
        <Button bsStyle="primary">Step +</Button>
        <Button bsStyle="primary">Step -</Button>
        </div>
      </GridLayout>
    )
  }
}


export default MyFirstGrid;