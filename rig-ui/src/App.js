import React, { Component } from 'react';
import './App.css';
import MyFirstGrid from './components/grid.jsx';
import {Navbar, Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';

class App extends Component {

  render() {
    return (
      
      <div className="App" style={{height: "100%"}}>
       <Navbar style={{background: "#333", borderRadius: "0px", border: "none"}}>
          <Navbar.Header>
            <Navbar.Brand>
              <img src="./uom.png" style={{width: "110px", height: "auto", marginLeft:"-165px", padding: "5px"}}/>
            </Navbar.Brand>
            <h3 style={{marginTop: "15px"}}>Tracking Rig Control Panel</h3>
          </Navbar.Header>
          <Nav pullRight>
            <NavItem eventKey={1} href="#">
              Link
            </NavItem>
            <NavItem eventKey={2} href="#">
              Link
            </NavItem>
            <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
              <MenuItem eventKey={3.1}>Action</MenuItem>
            </NavDropdown>
          </Nav>
        </Navbar>
        <MyFirstGrid/>
      </div>
    );
  }
}




export default App;