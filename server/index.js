var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var start;

var sendTime = function() {
  io.emit("time", ((Date.now()-start)/1000));
  setTimeout(sendTime, 1000);
}

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  console.log("device connected")

  socket.on('startAutoTest', function(msg){
    console.log("starting")
    start = Date.now();
    io.emit("startTest", "" + start)
    setTimeout(sendTime, 1000);
  }) 

  socket.on('stopAutoTest', function(msg){
    console.log("stoping")
    start = Date.now();
    io.emit("stopTest", "" + start)
    setTimeout(sendTime, 1000);
  }) 
  socket.on('enableRelay', function(x) {
    io.emit('relay'+ x + '', "")
  }) 

  socket.on("mainsOn", function(tf) {
      console.log("mainsOff")
      io.emit("mains", "")
  })
  
  socket.on('chat message', function(msg){
    console.log('message: ' + msg);
    if (msg === "start") {
      console.log("starting")
    start = Date.now();
    io.emit("startTest", "" + start)
    setTimeout(sendTime, 1000);
    } 
    if (msg === "stop") {
      end = Date.now();
      io.emit("stopTest", "" + end)
    } 
    if (msg === "mains") {
      end = Date.now();
      io.emit("mains", "" + end)
    } 
    if (msg === "1") {
      end = Date.now();
      io.emit("relay1", "" + end)
    } 
    if (msg === "2") {
      end = Date.now();
      io.emit("relay2", "" + end)
    } 
    if (msg === "3") {
      end = Date.now();
      io.emit("relay3", "" + end)
    } 
    if (msg === "4") {
      end = Date.now();
      io.emit("relay4", "" + end)
    } 
    if (msg === "5") {
      end = Date.now();
      io.emit("relay5", "" + end)
    } 
    if (msg === "q") {
      end = Date.now();
      io.emit("query")
    } 
  });

  io.on("test2", function() {
    console.log("input")
  });

});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

